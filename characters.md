# Characters Encountered

### Rose and Thorn

Two weird children who sent us into a house to remove the monsters. Inside the house we found some skeletons and ghosts who also claimed to be Rose and Thorn, but obviously dead.

### Strahd von Zarovitch

Killed everyone in the death house. Ruler of these parts, lives in castle ravenloft.

### Alenka, Mirabelle and Sylvia

Female women, dressed like Roma, say they're from around here.

### Irena

Woman.

### Donovich

Priest, seems a bit mad. Praying for his sick son.

### Doru

Sick son of Donovich.