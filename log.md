## 2018-06-05

- Found secret floor.
- Battering rammed into bolted shut door, found skeletons and encountered Ghosts of Rose and Thorn, they were locked in to protect them from whatever monster is around (seems sketchy, they may have just been locked in there), the Rose and Thorn that sent us in were not the ones up here.
- Found secret staircase downwards.
- Walked into a room that had human remains in it, cause of death unknown?
- Garimm smashed a chest and got something.
- Rick Hunter falls into spike trap and basically dies.
- I was somehow posessed by the ghost of Rose, expelling it made me feel better. Same applies to Frey.
- Used a table as a bridge to get over the spike trap.
- Garimm fell into the spike trap, but made it back out relatively not dead.

## 2018-06-15

- Voice chanting "He is the ancient, he is the land".
- Weird alcoves in room, filled with bones and mummified hands etc.
- Found dungeon with a skellington in it, Garimm pulls off finger with ring. (+25g for Garimm).
- Room with some refuse in a corner, chains dangle from the ceiling above an altar on a dais. 2ft high water in the room.
- 13 Apparitions with black hole torches appear on the ledges, they chant "One must die".
- A shambling mound attacks us. Seems big.
- Thorin almost gets one-hit (19 damage).
- Chandy gets slapped for 13.
- Portcullis slams into shambling mound and does a decent amount of damage.
- We're fighting the shambling mound trapped by the portcullis.
- Chandy dies, it's what happens when you overextend.
- Found two potions of healing in the corpse.
- Headed out of the house, the other floors now look dirty like the top floor of the house did.
- House now looks dilapidated from the outside as well.
- There are now worn down townsfolk around, town is no longer empty.
- We level up.

## 2018-06-22

- Candy is alive
- Melvin beconed us into his house
- Tells us we came out of the death house, people try burning it down but it seems quite durable. Dark arts people.
- We're surrounded by fog, can't leave.
- Gives us a Letter.

```
Hail to thee of might and valor.

I, a lowly servant of Barovia, send honor ło thee. We plead for thy so desperately needed assistance.

The love of my life, Ireena Kolyana, has been afflicted by an evil so deadly that even the good people of our village cannot protect her. She languishes from her wound, and I would have her saved from this menace.

There is much wealth in this community. I offer all that might be had to thee and thy fellows if thou shalt but answer my desperate plea.

Come quickly, for her time is at hand! All that I have shall be thine!

Kolyan Indirovich
Burgomasłer
```

- Tavern turned super dusty and looks mega old. Outside of building looks abandoned.
- We wander westwards for about 5 hours. We see gates attached to a rock wall, flanked by forests.
- We walk through the gate, everything is creepy and scary and it smells like death.
- Corpse in a clearing, we get attacked by wolves.
- Corpse has a letter.
- Letter has the same date as other letter, handwriting looks the same. Both letters dated 4 days ago.

```
Hail thee of might and valor:

I, the Burgomaster of Barovia, send you honor—with despair.

My adopted daughter, the fair Ireena Kolyana, has been these past nights bitten by a vampyr. For over four hundred years, this creature has drained the life blood of my people. Now, my dear Ireena languishes and dies from an unholy wound caused by this vile beast. He has become too powerful to conquer.

So I say to you, give us up for dead and encircle this land with the symbols of good. Let holy men call upon their power that the devil may be contained within the walls of weeping Barovia. Leave our sorrows to our graves, and save the world from this evil fate of ours.

There is much wealth entrapped in this community. Return for your reward after we are all departed for a better life.

Kolyan Indirovich
Burgomaster
```

- Enter barovia, sound of wailing in the distance.


## 2018-07-05 
- Went to burgomasters house, were sent to the pub.
- Blood of the vine tavern was renamed to blood on the vine tavern.
- People in the tavern were not friendly
- Go to burgomasters house, he is dead and a cask. Slight rotting smell.
- Strahd killed the burgomaster, Son and Daughter want to leave and ask us for help.
- Walked into chapel, priest is a bit mad, praying to save his son doru. 
- Turns out, Doru is a vampire fledgling something. DUN DUN DUN.
- We killed him.
- We dig a grave for burgomaster and doru.
 

## 2018-07-13
- Walked out of barovia along the road
- Fighting Ravens.
- We killed the raven. They were there for food.
- Thorin sees Thorin hanging from the gallows. Everyone else sees a ghost.
- We head along the path towards the river, it's a pretty small road/path.
- We run into a bunch of travellers.
- They tell us about a Wizard and a Vampire (Strahd) fighting for the castle of Ravenloft.
- We get sent to madam Eva, she's a Crohn.
- She shows us some Tarot cards
- Fate guided us here, apparently.
- Actually, we're here by strahds will, to leave we need to fight him, there are things that will help us along the way and those things are hidden in the tarot card.
- She's fortune telling so she might be full of bat guano.
